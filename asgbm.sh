#!/bin/bash

mkdir $1
cd $1
git init
#git branch -m main
touch README.md
git add README.md
git commit -m "Initial commit"
git tag -a 0.1 -m "0.1"

# Moving to new branch develop
git checkout -b develop
touch develop01
git add develop01
git commit -m "Added develop01"
touch develop02
git add develop02
git commit -m "Added develop02"
touch develop03
git add develop03
git commit -m "Added develop03"

# Moving to new branch feature/future_release

git checkout -b feature/future_release develop
touch feature_future01
git add feature_future01
git commit -m "Added feature_future01"
touch feature_future02
git add feature_future02
git commit -m "Added feature_future02"


git checkout develop


# Moving to new branch feature/next_release
git checkout -b feature/next_release develop

touch feature_next01
git add feature_next01
git commit -m "Added feature_next01"
touch feature_next02
git add feature_next02
git commit -m "Added feature_next02"
touch feature_next03
git add feature_next03
git commit -m "Added feature_next03"

# Go back to develop and add 3 more commits

git checkout develop
touch develop04
touch develop05


git add develop04
git commit -m "Added develop04"
git add develop05
git commit -m "Added develop05"

# Go back to main to create hotfixes branch
# create a commit and merge it to develop and main

#
git checkout master
git checkout -b hotfixes


touch hotfix01
git add hotfix01
git commit -m "Added hotfix01"
git checkout develop
git merge hotfixes --no-ff --no-edit

##
git checkout master
git merge hotfixes --no-ff --no-edit
git tag -a 0.2 -m "0.2"

# Merge feature/next into develop

git checkout develop
git merge feature/next_release --no-ff --no-edit

# Merge develop into release
# Start of release branch for 1.0

git branch release 
git checkout release


touch release01
git add release01
git commit -m "Start of release branch for 1.0"

git merge develop --no-ff --no-edit

touch release02
git add release02
git commit -m "Added release02"

# Merge release into develop

git checkout develop
git merge release --no-ff --no-edit

# Add third feature for future release

git checkout feature/future_release

touch feature_future03
git add feature_future03
git commit -m "Added feature_future03"








#touch develop06
#git add develop06
#git commit -m "Added develop06"





